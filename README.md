# Migrantech: INCIDINMX

Integración Ciudadana Inteligente del Migrante en México

# Requisitos de instalación

 -  Ubuntu LAMP on 14.04
# Comandos de Instalación
 1.  Install Apache  
`sudo apt-get update`
`sudo apt-get install apache2`
 2. Install MySQL
`sudo apt-get install mysql-server libapache2-mod-auth-mysql php5-mysql`
 3.  Install PHP
`sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt`
4.  Reiniciar server
`sudo service apache2 restart`
5. Clonar repositorio
`cd /var/www/`
`git clone https://gitlab.com/j_rico/migranTech-Retos-Publicos`


# Licencia

GNU GENERAL PUBLIC LICENSE

Licensed under the  [GPLv3](https://gitlab.com/j_rico/migranTech-Retos-Publicos/blob/master/COPYING)  License.

-------------------------