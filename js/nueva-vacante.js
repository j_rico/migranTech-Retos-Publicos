$(document).ready(function () {
    $('#character-btn').click(e => {
        $('#panel-data').addClass('out-left')
        $('#panel-pick').removeClass('out-right')
    })
   /*  $('#panel-data').addClass('out-left')
    $('#panel-pick').removeClass('out-right') */
    $('#panel-pick').show()

    var $flowchart = $('.canvas-panel').flowchart({
        data: flowData,
        // canUserEditLinks: false,
        // canUserMoveOperators: false
        multipleLinksOnOutput: true
    })

    $('#new-item-btn').click(e => {
        let operatorTitle = $('#item-name-input').val()

        if (operatorTitle == '') return
        let operatorId = 'created_' + itemCount++
        let operatorData = {
            top: $('.canvas-grid').height() - 25,
            left: 25,
            properties: {
                title: operatorTitle,
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        }
        $('#item-name-input').val('')
        $flowchart.flowchart('createOperator', operatorId, operatorData)
    })

    $('#delete-item-btn').click(e => {
        $flowchart.flowchart('deleteSelected')
    })

    $('#save-btn, #save-back-btn').click(e => {
        $('#panel-data').removeClass('out-left')
        $('#panel-pick').addClass('out-right')
    })
})

var itemCount = 0
