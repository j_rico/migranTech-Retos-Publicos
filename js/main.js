$(document).ready(function () {
  var sound = new Audio('recurso/audio.wav'); 
  var backgroundSound = new Audio('recurso/audio.wav'); 
  var playing = false;

  backgroundSound.play(); 
    $('#registroID').click(function (e) {
        //e.preventDefault()
        /* $('#registroPanel').show()
        $('#sesionPanel').hide() */
    })

    $('#play').click(function (e) {
        e.preventDefault()
       
        if(playing){
            $('#iconoRepro').attr("class","fas fa-play-circle fa-2x");
            sound.pause();
            playing = false;
            backgroundSound.play();
        }
        else {
            $('#iconoRepro').attr("class","far fa-stop-circle fa-2x");
            sound.play();
            playing = true;
            backgroundSound.pause();
        }

    })
})