var flowData = {
    operators: {
        // Propositos Clave
        operator0: {
            top: 300,
            left: 40,
            properties: {
                title: 'COORDINAR AL EQUIPO Y ADMINISTRAR',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        // Funcion Principal
        operator1: {
            top: 100,
            left: 260,
            properties: {
                title: 'INTELIGENCIA',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        operator2: {
            top: 240,
            left: 260,
            properties: {
                title: 'LIDERAZGO',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        // Funcion Básica
        operator3: {
            top: 80,
            left: 480,
            properties: {
                title: 'CONOCIMIENTOS MATEMÁTICOS',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        operator4: {
            top: 140,
            left: 480,
            properties: {
                title: 'MANEJO DE COSTOS',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        operator5: {
            top: 220,
            left: 480,
            properties: {
                title: 'ACCIÓN MOTIVADORA',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        operator6: {
            top: 280,
            left: 480,
            properties: {
                title: 'CAPACITAR AL EQUIPO',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        // Sub Funcion
        operator7: {
            top: 80,
            left: 720,
            properties: {
                title: 'HACER FORMULAS SIMPLES',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        operator8: {
            top: 140,
            left: 720,
            properties: {
                title: 'CUIDAR LOS GASTOS Y GANANCIAS',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        operator9: {
            top: 220,
            left: 720,
            properties: {
                title: 'INTEGRAR AL EQUIPO / NUEVOS',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        },
        operator10: {
            top: 280,
            left: 720,
            properties: {
                title: 'SUPERVISAR PERSONALMENTE',
                inputs: {
                    input_1: {
                        label: ''
                    }
                },
                outputs: {
                    output_1: {
                        label: ''
                    }
                }
            }
        }
    }
}
