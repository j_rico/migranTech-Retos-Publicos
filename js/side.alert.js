$(document).ready(function () {
    sa = new SideAlert('.side-alert-main')

    $('#message-box').click(e => sa.togglePanel())
})

class SideAlert {
    constructor (mainContainer) {
        this.mainContainer = mainContainer
        this.$panel = null

        this.init()
    }

    init () {
        let $cont = $(this.mainContainer)
        $cont.append(`
            <div id="side-alert-wrapper"></div>
        `)

        this.$panel = $cont.find('#side-alert-wrapper')
        
        this.$panel.hide()
        this.populateMessages()
    }

    showMessagePanel () {
        this.$panel.empty()
        this.populateMessages()
        this.$panel.fadeIn('3000')
        this.$panel.addClass('sa-in')
    }

    hideMessagePanel () {
        this.$panel.fadeOut('3000')
        this.$panel.removeClass('sa-in')
        $('#side-alert-wrapper').attr('style', '')
        $('.message').css('style', '')
    }

    togglePanel () {
        if (this.$panel.is(':visible')) {
            this.hideMessagePanel()
        } else {
            this.showMessagePanel()
        }
    }

    populateMessages () {
        for (let index = 0; index < 20; index++) {
            this.$panel.append(`
                <div class="message">
                    <span class="dissmiss">
                        <i class="fas fa-times"></i>
                    </span>
                    <span class="timer"> 10:25 </span>
                    <div class="icon">
                        <img src="../recurso/INCIDIN.png">
                    </div>
                    <div class="body">
                        <div class="remitter">
                            <h4 class="remitter-name">asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasd</h4>
                        </div>
                        <div class="content">
                            <p class="text"> asdasdasd asdas asd as das da sda sd </p>
                        </div>
                    </div>
                    <div class="actions">
                        <div>
                        </div>
                    </div>
                </div>
            `)
        }

        let messagesArray = this.$panel.find('.message')
        for (let index = 0; index < 20; index++) {
            setTimeout(() => {
                $(messagesArray[index]).addClass('sa-in')
            }, 50 * index)
        }

        /* $('.message').click(e => {
            $('#side-alert-wrapper').css('width', '60%')
            $('#side-alert-wrapper').css('align-items', 'start')
            $('.message').css('width', '45%')
        }) */
    }
}
